#!/usr/bin/python3

import click
import requests
import json
import pandas as pd
from datetime import datetime
from influxdb import InfluxDBClient
from time import sleep

INFLUX_HOST = "dashdot.local"
INFLUX_PORT = 8086
url = 'https://www.data.brisbane.qld.gov.au/data/api/3/action/datastore_search?resource_id=78c37b45-ecb5-4a99-86b2-f7a514f0f447&limit=200'  
LOOKUP = {
    "E1594": "BreakfastCkMouth",
    "E1525": "BowenHillsStream",
    "E1524": "BowenHillsRain",
    "E1531": "BancroftPark",
    "E1884": "Bardon",
    "E1710": "Ashgrove",
    "E1575": "GordonParkRain",
}


@click.command()
def river_data_to_influx():
    # TODO No exception handling anywhere!
    # TODO use logging
    response = requests.get(URL)
    print("[{}] Creek data request code: {}".format(datetime.now(), response))
    results = json.loads(response.text)
    influx_json = []
    for data in results["result"]["records"]:
        date = (
            pd.to_datetime(data["Measured"])
            .tz_localize("Australia/Brisbane")
            .tz_convert("UTC")
            .strftime("%Y-%m-%dT%H:%m:%S")
        )
        for k, v in LOOKUP.items():
            if data[k] != "-":
                influx_json.append(
                    {
                        "measurement": v,
                        "time": date,
                        "fields": {"height": float(data[k])},
                    }
                )
    client = InfluxDBClient(host=INFLUX_HOST, port=INFLUX_PORT)
    client.switch_database("tides")
    influx_result = client.write_points(influx_json)
    print(
        "[{}] Influx write {}".format(
            datetime.now(), "Succeeded" if influx_result else "Failed"
        )
    )
    sleep(2) # Just in case it needs time to close out the client connection
    return 0


if __name__ == "__main__":
    exit(river_data_to_influx())
